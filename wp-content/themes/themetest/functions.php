<?php
//https://developer.wordpress.org/reference/hooks/after_setup_theme/
add_action("after_setup_theme", function () {
    //https://developer.wordpress.org/reference/functions/add_theme_support/
    add_theme_support('title-tag');
    //https://developer.wordpress.org/reference/functions/add_action/
    add_action("wp_head", function () {
        //die("hard2");
    });
});

//https://developer.wordpress.org/reference/hooks/wp_enqueue_scripts/
add_action("wp_enqueue_scripts", function () {

    //https://developer.wordpress.org/reference/functions/wp_register_script/
    wp_register_script(
        "popper",
        "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",
        [],
        false,
        true
    );
    wp_register_script(
        "slim",
        "https://code.jquery.com/jquery-3.4.1.slim.min.js",
        [],
        false,
        true
    );

    //https://developer.wordpress.org/reference/functions/wp_enqueue_style/
    wp_enqueue_style(
        "bootstrap",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    );
    //https://developer.wordpress.org/reference/functions/wp_enqueue_script/
    wp_enqueue_script(
        "bootstrap",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js",
        [
            "slim",
            "popper"
        ],
        false,
        true
    );

});