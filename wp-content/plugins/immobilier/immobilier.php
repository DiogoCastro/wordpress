<?php
/**
 * @package Immobilier
 */
/*
Plugin Name: Immobilier
Plugin URI: https://bitbucket.org/DiogoCastro/wordpress/src/master/wp-content/plugins/immobilier/
Description: Simple plugin - test h2a
Version: 1
Author: Diogo Castro
*/

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

class Immobilier
{
    function __construct()
    {
        //add_action('init', [$this, 'custom_post_type']);
    }

    public function register()
    {
        add_action('admin_menu', [$this, 'add_admin_pages']);
        add_shortcode('immobiliers', [$this, 'insertedImmobiliers']);
    }

    public function add_admin_pages()
    {
        add_menu_page(
            'Immobilier Plugin',
            'Immobilier',
            'manage_options',
            'immobilier_plugin',
            [$this, 'adminIndex'],
            '',
            110
        );

        add_submenu_page(
            'immobilier_plugin',
            'Immobilier Plugin',
            'Add/Edit Immo',
            'manage_options',
            'manage_immobilier_plugin',
            [$this, 'subAdminIndex'],
            1
        );

        add_submenu_page(
            'immobilier_plugin',
            'Immobilier Type Plugin',
            'Add/Edit Immo Type',
            'manage_options',
            'manage_immobilier_type_plugin',
            [$this, 'subAdminImoTypeIndex'],
            1
        );
    }

    public function adminIndex()
    {
        require_once plugin_dir_path(__FILE__) . 'views/admin.html';
    }

    public function subAdminIndex()
    {
        require_once plugin_dir_path(__FILE__) . 'views/addEditImo.html';
    }

    public function subAdminImoTypeIndex()
    {
        require_once plugin_dir_path(__FILE__) . 'views/addEditImoType.html';
    }

    function activate()
    {
        include plugin_dir_path(__FILE__) . 'controllers/setup.php';
        global $wpdb;
        $a = new Setup($wpdb);
        $a->addTables();
    }

    function desactivate()
    {
        include plugin_dir_path(__FILE__) . 'controllers/setup.php';
        global $wpdb;
        $a = new Setup($wpdb);
        $a->removeTables();
    }

    function custom_post_type()
    {
        register_post_type('book', ['public' => true, 'label' => 'books']);
    }

    //displays immobiliers inserted in bd
    function insertedImmobiliers()
    {
        require_once plugin_dir_path(__FILE__) . 'views/front.html';
    }
}

if (class_exists('Immobilier')) {
    $immobilier = new Immobilier();
    $immobilier->register();
}

register_activation_hook(__FILE__, [$immobilier, 'activate']);
register_deactivation_hook(__FILE__, [$immobilier, 'desactivate']);

//https://developer.wordpress.org/reference/hooks/wp_enqueue_scripts/
add_action("wp_enqueue_scripts", function () {

    //https://developer.wordpress.org/reference/functions/wp_register_script/
    wp_register_script(
        "popper",
        "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",
        [],
        false,
        true
    );
    wp_register_script(
        "slim",
        "https://code.jquery.com/jquery-3.4.1.slim.min.js",
        [],
        false,
        true
    );

    //https://developer.wordpress.org/reference/functions/wp_enqueue_style/
    wp_enqueue_style(
        "bootstrap",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    );
    //https://developer.wordpress.org/reference/functions/wp_enqueue_script/
    wp_enqueue_script(
        "bootstrap",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js",
        [
            "slim",
            "popper"
        ],
        false,
        true
    );

});
