<?php


/**
 * Class Front
 */
class Front
{

    /**
     * Returns all the active immobiliers
     *
     * @param $wpdb
     * @return array
     */
    public function getActiveImos($wpdb): array
    {
        return $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM wp_houses WHERE isActive = %d", true),
            ARRAY_A
        );
    }
}

if (class_exists('Front')) {
    global $wpdb;
    $immobilier = new Front();

    $results = $immobilier->getActiveImos($wpdb);
}
