<?php

/**
 * Class Setup
 */
class Setup
{
    /** @var wpdb $dbRun */
    private $dbRun;

    /** @var string */
    private $tableName;

    /**
     * Setup constructor.
     * @param wpdb $wpdb
     */
    function __construct(wpdb $wpdb)
    {
        $this->dbRun = $wpdb;
        $this->tableName = "wp_houses";
    }

    /**
     * Create and insert all necessary data for the plugin to work
     */
    public function addTables()
    {
        //drop tables if they exist
        $this->dbRun->query("DROP TABLE IF EXISTS wp_houses");
        $this->dbRun->query("DROP TABLE IF EXISTS wp_houses_types");

        $this->dbRun->query("
            CREATE TABLE IF NOT EXISTS `wp_houses` (
             `id` int(11) NOT NULL AUTO_INCREMENT,
             `sTitle` varchar(100),
             `sCity` varchar(100),
             `dArea` float,
             `dPrice` float,
             `dImoId` int,
             `dType` int,
             `sPictureName` varchar(255),
             `isActive` boolean,
             `isPublish` boolean,
             PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->dbRun->query("
            CREATE TABLE IF NOT EXISTS `wp_houses_types` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `sType` varchar(100),
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ");

        $this->dbRun->query("
            INSERT INTO `wp_houses_types`
            VALUES
            (null,'Appartement'),
            (null,'Maison'),
            (null,'Terrain'),
            (null,'Commerce'),
            (null,'Immeuble'),
            (null,'Bureau'),
            (null,'Bateau'),
            (null,'Locaux d activite / Entrepots'),
            (null,'Cave / Box')
        ");

        //insert all entries from json file to table wp_houses
        if (file_exists(__DIR__ . "/../scripts/immobiliers.json")) {
            $string = file_get_contents(__DIR__ . "/../scripts/immobiliers.json");
            $jsonImos = json_decode($string, true);

            $sql = "INSERT INTO wp_houses VALUES ";

            foreach ($jsonImos["properties"] as $item) {
                $val = isset($item["price"]["value"]) ? $item["price"]["value"] : null;
                $image = isset($item["pictures"][0]["url"]) ? $item["pictures"][0]["url"] : null;
                $sql .= "(null, '" . $item["name"] . "','" . $item["city"]["name"] . "','" . $item["area"]["value"] .
                    "','" . $val . "'," . $item["id"] . "," . $item["type"] . ",'".$image."',1, 1 ), ";
            }

            $sql = rtrim($sql, ", ");
            $this->dbRun->query($sql);
        }
    }

    /**
     * Delete all tables from the plugin
     */
    public function removeTables()
    {
        $this->dbRun->query("DROP TABLE IF EXISTS `wp_houses`");
        $this->dbRun->query("DROP TABLE IF EXISTS `wp_houses_types`");
    }

}
