<?php


/**
 * Class ImoAdmin
 */
class Admin
{
    /** @var wpdb $dbRun */
    private $dbRun;

    /** @var string */
    private $tableName;

    /** @var string */
    private $tableTypeName;

    /**
     * Admin constructor.
     * @param wpdb $wpdb
     */
    function __construct(wpdb $wpdb)
    {
        $this->dbRun = $wpdb;
        $this->tableName = "wp_houses";
        $this->tableTypeName = "wp_houses_types";
    }

    /**
     * Inserts or Updates one immobilier into table wp_houses
     *
     * @return string
     */
    public function postImo(): string
    {
        $data = [
            'sTitle' => isset($_POST['title']) ? sanitize_text_field($_POST['title']) : "",
            'sCity' => isset($_POST['city']) ? sanitize_text_field($_POST['city']) : "",
            'dArea' => $_POST['area'],
            'dPrice' => isset($_POST['price']) ? $_POST['price'] : "",
            'dImoId' => $_POST['imoId'],
            'dType' => $_POST['type'],
            'isActive' => true
        ];

        //file upload
        if (isset($_FILES['fileToUpload']) && $_FILES['fileToUpload']["size"] > 0) {
            if (!function_exists('wp_handle_upload')) {
                require_once(ABSPATH . 'wp-admin/includes/file.php');
            }

            $uploadedfile = $_FILES['fileToUpload'];

            $upload_overrides = [
                'test_form' => false
            ];

            add_filter('upload_dir', [$this, 'setCustomPictureFolder']);
            $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
            //set filename to the correct one
            $data["sPictureName"] = basename($movefile['file']);
            remove_filter('upload_dir', [$this, 'setCustomPictureFolder']);

            /*
            if ($movefile && !isset($movefile['error'])) {
                echo "File is valid, and was successfully uploaded.\n";
            } else {
                echo "Error uploading file, contact system admin.\n";
            }
            */
        }

        $format = ['%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'];

        if (isset($_POST['add'])) {
            $this->dbRun->insert($this->tableName, $data, $format);
        } else {
            $where = ['dImoId' => $_POST['imoId']];
            $this->dbRun->update($this->tableName, $data, $where, $format);
        }

        $my_id = $this->dbRun->insert_id;

        if ($my_id > 0) {
            return 'Inserted successfully';
        }

        return 'Failed to insert immobilier';
    }

    /**
     * Insert a new type of immobilier
     *
     * @return string
     */
    public function postImoType(): string
    {
        $newImoType = sanitize_text_field($_POST["sType"]);

        $this->dbRun->insert($this->tableTypeName, ["sType" => $newImoType], ["%s","%s"]);

        $my_id = $this->dbRun->insert_id;

        if ($my_id > 0) {
            return 'Inserted successfully';
        }

        return 'Failed to insert immobilier';
    }

    /**
     * Insert a new type of immobilier
     *
     * @return string
     */
    public function editImoType(): string
    {
        if (isset($_POST["sType"]) && !empty($_POST["sType"])) {
            foreach ($_POST["sType"] as $k=>$v) {
                $this->dbRun->query("UPDATE wp_houses_types SET sType = '".sanitize_text_field($v[0])."' WHERE id = $k");
            }
            $my_id = $this->dbRun->insert_id;

            if ($my_id > 0) {
                return 'Types de immobiliers edited with successfully';
            }
        }

        return 'Failed to edit immobilier types';
    }

    /**
     * Set a custom folder for the image uploads
     *
     * @param $dir
     * @return array
     */
    public function setCustomPictureFolder($dir): array
    {
        return [
                'path' => $dir['basedir'] . '/immobiliers',
                'url' => $dir['baseurl'] . '/immobiliers',
                'subdir' => '/immobiliers',
            ] + $dir;
    }

    /**
     * @param string $dir
     * @param string $name
     * @param string $ext
     * @return int
     */
    function fixPictureFileName($dir, $name, $ext): int
    {
        return isset($_POST['imoId']) ?: $_POST['imoId'];
    }

    /**
     * Do bulk actions
     *
     * @return int
     */
    public function doBulkAction(): int
    {
        $actionToDo = sanitize_text_field($_POST["action"]);
        $imosToChange = $_POST["check"];
        $imos = "";

        foreach ($imosToChange as $imosToChangeItem) {
            $imos .= "$imosToChangeItem".",";
        }
        $imos = rtrim($imos, ",");

        if ($actionToDo === "activate-selected") {
            return $this->dbRun->query("UPDATE wp_houses SET isActive = true WHERE dImoId IN ($imos)");
        }
        elseif ($actionToDo === "deactivate-selected") {
            return $this->dbRun->query("UPDATE wp_houses SET isActive = false WHERE dImoId IN ($imos)");
        }
        elseif ($actionToDo === "publish-selected") {
            return $this->dbRun->query("UPDATE wp_houses SET isPublish = true WHERE dImoId IN ($imos)");
        }
        elseif ($actionToDo === "unpublish-selected") {
            return $this->dbRun->query("UPDATE wp_houses SET isPublish = false WHERE dImoId IN ($imos)");
        }
    }

    /**
     * Get new dImoId for a new immobilier
     *
     * @return int
     */
    public function getMaxId(): int
    {
        return $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT MAX(dImoId) as 'newId' FROM wp_houses WHERE dImoId > %d", 0),
            ARRAY_A
        )[0]['newId']+1;
    }

    /**
     * Returns all the immobiliers active
     *
     * @return array
     */
    public function getActiveImos(): array
    {
        return $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT * FROM wp_houses WHERE isActive = %d", true),
            ARRAY_A
        );
    }

    /**
     * Returns all immobiliers
     *
     * @param int $offset
     * @param int $limit
     * @param string $search
     * @param string $orderBy
     * @return array
     */
    public function getAllImos($offset, $limit, $search, $orderBy): array
    {
        $search = "%".$search."%";
        return $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT * FROM wp_houses WHERE sTitle LIKE %s OR sCity LIKE %s ORDER BY $orderBy LIMIT %d, %d", $search, $search, $offset, $limit),
            ARRAY_A
        );
    }

    /**
     * Returns total of immobiliers in bd
     *
     * @param string $search
     * @return int
     */
    public function getTotalImos($search): int
    {
        $search = "%".$search."%";
        $res = $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT count(*) as total FROM wp_houses WHERE sTitle LIKE %s OR sCity LIKE %s", $search, $search),
            ARRAY_A
        )[0]["total"];

        if (!is_null($res) && is_numeric($res)) {
            return $res;
        }

        return 0;
    }

    /**
     * Returns all immobiliers published
     *
     * @param int $offset
     * @param int $limit
     * @param string $search
     * @param string $orderBy
     * @return array
     */
    public function getPublishImos($offset, $limit, $search, $orderBy): array
    {
        $search = "%".$search."%";
        return $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT * FROM wp_houses WHERE isPublish = %d AND (sTitle LIKE '%s' OR sCity LIKE '%s') ORDER BY $orderBy LIMIT %d, %d", true, $search, $search, $offset, $limit),
            ARRAY_A
        );
    }

    /**
     * Returns total of published immobiliers
     *
     * @param string $search
     * @return int
     */
    public function getTotalPublishImos($search): int
    {
        $search = "%".$search."%";
        $res = $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT count(*) as total FROM wp_houses WHERE isPublish = %d AND isActive = %d AND (sTitle LIKE '%s' OR sCity LIKE '%s')", true, true, $search, $search),
            ARRAY_A
        )[0]["total"];

        if (!is_null($res) && is_numeric($res)) {
            return $res;
        }

        return 0;
    }

    /**
     * Returns all inactive immobiliers
     *
     * @param int $offset
     * @param int $limit
     * @param string $search
     * @param string $orderBy
     * @return array
     */
    public function getInactiveImos($offset, $limit, $search, $orderBy): array
    {
        $search = "%".$search."%";
        return $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT * FROM wp_houses WHERE isActive = %d AND (sTitle LIKE '%s' OR sCity LIKE '%s') ORDER BY $orderBy LIMIT %d, %d", false, $search, $search, $offset, $limit),
            ARRAY_A
        );
    }

    /**
     * Returns total of inactive immobiliers
     *
     * @param string $search
     * @return int
     */
    public function getTotalInactiveImos($search): int
    {
        $search = "%".$search."%";
        $res = $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT count(*) as total FROM wp_houses WHERE isActive = %d AND (sTitle LIKE '%s' OR sCity LIKE '%s')", false, $search, $search),
            ARRAY_A
        )[0]["total"];

        if (!is_null($res) && is_numeric($res)) {
            return $res;
        }

        return 0;
    }

    /**
     * Returns a specific immobilier
     *
     * @param int $id
     * @return array
     */
    public function getImoById($id): array
    {
        return $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT sTitle, dImoId, sCity, dArea, dPrice, dImoId, dType, sPictureName FROM wp_houses WHERE dImoId = %d", $id),
            ARRAY_A
        );
    }

    /**
     * Returns a specific type de immobilier
     *
     * @param int $id
     * @return array
     */
    public function getImoType($id): array
    {
        return $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT id, sType FROM wp_houses_types WHERE id = %d", $id),
            ARRAY_A
        )[0];
    }

    /**
     * Returns all types de immobiliers
     *
     * @return array
     */
    public function getImoTypes(): array
    {
        return $this->dbRun->get_results(
            $this->dbRun->prepare("SELECT id, sType FROM wp_houses_types WHERE id > %d", 0),
            ARRAY_A
        );
    }

    /**
     * Change isActive status for a specific immobilier
     *
     * @return bool|int
     */
    public function changeImoStatus()
    {
        //check if number is a valid int
        if ( filter_var($_REQUEST['changestatus'], FILTER_VALIDATE_INT)) {
            $imoId = $_REQUEST['changestatus'];

            return $this->dbRun->query("UPDATE wp_houses SET isActive = IF(isActive = true, false, true) WHERE dImoId = $imoId");
        }

        return false;
    }

    /**
     * Change isPublish status for a specific immobilier
     *
     * @return bool|int
     */
    public function changeImoPublishStatus()
    {
        //check if number is a valid int
        if ( filter_var($_REQUEST['changepublish'], FILTER_VALIDATE_INT)) {
            $imoId = $_REQUEST['changepublish'];

            return $this->dbRun->query("UPDATE wp_houses SET isPublish = IF(isPublish = true, false, true) WHERE dImoId = $imoId");
        }

        return false;
    }
}

if (class_exists('Admin')) {
    global $wpdb;

    $immobilier = new Admin($wpdb);

    //search value
    $search = isset($_POST['searchVal']) ? sanitize_text_field($_POST['searchVal']) : "%";
    $orderBy = isset($_REQUEST["orderby"]) ? sanitize_text_field($_REQUEST["orderby"]) : "sTitle";
    $order = isset($_REQUEST["order"]) ? sanitize_text_field($_REQUEST["order"]) : "asc";
    $ord = $orderBy . " " . $order;
    $new_order = $order === "desc" ? "asc" : "desc";

    if (isset($_POST['edit'])) {
        if($immobilier->postImo()) {
            $result = "<div id='no-items-selected' class='notice is-dismissible'><p>Immobilier edited</p><button type='button' class='notice-dismiss'>
            <span class='screen-reader-text'>Dismiss this notice.</span></button></div>";
        };
    }

    if (isset($_POST['bulk'])) {
        if (!isset($_POST["action"])) {
            $result = "<div id='no-items-selected' class='notice notice-error is-dismissible'><p>None bulk action selected</p><button type='button' class='notice-dismiss'>
            <span class='screen-reader-text'>Dismiss this notice.</span></button></div>";
        }
        elseif (!isset($_POST["check"])) {
            $result = "<div id='no-items-selected' class='notice notice-error is-dismissible'><p>None immobilier selected</p><button type='button' class='notice-dismiss'>
            <span class='screen-reader-text'>Dismiss this notice.</span></button></div>";
        }
        else {
            if ($immobilier->doBulkAction()) {
                $result = "<div id='no-items-selected' class='notice is-dismissible'><p>Action executed with success</p><button type='button' class='notice-dismiss'>
            <span class='screen-reader-text'>Dismiss this notice.</span></button></div>";
            }
        }
    }

    if (isset($_POST['add'])) {
        if($immobilier->postImo()) {
            $result = "<div id='no-items-selected' class='notice is-dismissible'><p>Immobilier created</p><button type='button' class='notice-dismiss'>
            <span class='screen-reader-text'>Dismiss this notice.</span></button></div>";
        };
    }

    if (isset($_POST['addtype'])) {
        if($immobilier->postImoType()) {
            $result = "<div id='no-items-selected' class='notice is-dismissible'><p>Type de immobilier created</p><button type='button' class='notice-dismiss'>
            <span class='screen-reader-text'>Dismiss this notice.</span></button></div>";
        };
    }

    if (isset($_POST['editype'])) {
        if($immobilier->editImoType()) {
            $result = "<div id='no-items-selected' class='notice is-dismissible'><p>Type de immobilier Edited</p><button type='button' class='notice-dismiss'>
            <span class='screen-reader-text'>Dismiss this notice.</span></button></div>";
        };
    }

    if (isset($_REQUEST['changestatus'])) {
        if($immobilier->changeImoStatus()) {
            $result = "<div id='no-items-selected' class='notice is-dismissible'><p>Status changed</p><button type='button' class='notice-dismiss'>
            <span class='screen-reader-text'>Dismiss this notice.</span></button></div>";
        }
    }

    if (isset($_REQUEST['changepublish'])) {
        if($immobilier->changeImoPublishStatus()) {
            $result = "<div id='no-items-selected' class='notice is-dismissible'><p>Status changed</p><button type='button' class='notice-dismiss'>
            <span class='screen-reader-text'>Dismiss this notice.</span></button></div>";
        }
    }

    if (isset($_REQUEST['edit']) && is_numeric($_REQUEST['edit'])) {
        $id = filter_var($_REQUEST['edit'], FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]);
        $imoToEdit = $immobilier->getImoById($id)[0];
    }

    $limit = 5; // number of rows in page

    //search offset
    $offset = (isset($_REQUEST["pagenum"]) ? sanitize_text_field($_REQUEST["pagenum"]) * $limit - $limit : 0);

    $totalImos = $immobilier->getTotalImos($search);
    $totalPublishImos = $immobilier->getTotalPublishImos($search);
    $totalInactiveImos = $immobilier->getTotalInactiveImos($search);

    //top filters search
    if (isset($_REQUEST["imos_status"])) {
        $filter = sanitize_text_field($_REQUEST["imos_status"]);
        if ($filter === "all") {
            $jsonImos = $immobilier->getAllImos($offset, $limit, $search, $ord);
            $total = $totalImos;
        } elseif ($filter === "publish") {
            $jsonImos = $immobilier->getPublishImos($offset, $limit, $search, $ord);
            $total = $totalPublishImos;
        } else {
            $jsonImos = $immobilier->getInactiveImos($offset, $limit, $search, $ord);
            $total = $totalInactiveImos;
        }

        $current = $filter;
    } else {
        $jsonImos = $immobilier->getAllImos($offset, $limit, $search, $ord);
        $total = $totalImos;
        $current = "all";
    }

    $pagenum = $pagenum = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;
    $num_of_pages = ceil($total / $limit);

    $page_links = paginate_links(array(
        'base' => add_query_arg('pagenum', '%#%'),
        'format' => '',
        'prev_text' => __('&laquo; Previous'),
        'next_text' => __('Next &raquo;'),
        'total' => $num_of_pages,
        'current' => $pagenum
    ));
}
