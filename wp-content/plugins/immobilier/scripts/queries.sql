CREATE TABLE IF NOT EXISTS wordpress.`wp_houses` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `sTitle` varchar(100),
 `sCity` varchar(100),
 `dArea` float,
 `dPrice` float,
 `dImoId` int,
 `dType` int,
 `sPictureName` varchar(100),
 `isActive` boolean,
 `isPublish` boolean,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS wordpress.`wp_houses_types` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `sType` varchar(100),
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO wordpress.`wp_houses_types`
VALUES
(null,"Appartement"),
(null,"Maison"),
(null,"Terrain"),
(null,"Commerce"),
(null,"Immeuble"),
(null,"Bureau"),
(null,"Bateau"),
(null,"Locaux d activite/Entrepots"),
(null,"Cave/Box");