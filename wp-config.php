<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Naj4J8tN(Fm24LzS8_]w!01v.v|+0,|E~N,TC1=E,3kNLFrT2QJL_ !4Ww_;q)m}' );
define( 'SECURE_AUTH_KEY',  '<.:ESj]F7?[?oj>!3jITzYv;D8*v;C{|Ll_`_@#y}rQpa,?U/_RN|P_w{yp2_#Ui' );
define( 'LOGGED_IN_KEY',    'Cg]uC;i7X(Vw@#C@5p J:{<>KU3zw|JVD8=CU)g7QOL<tP@:RiHnYj39bW-IU4]w' );
define( 'NONCE_KEY',        '6w 3H^#gf?z&(z-~*MCz_B+|V2lKhT`h|m)N<D:)tQ8) E?)>xsuNdeg[Bq!pa7e' );
define( 'AUTH_SALT',        'sRBr`<d~Jcmhv|4:l(0?3 4*QwVpM~`k(#)${0,&4kIVr5M 4>m3(N:PX1gl1SRE' );
define( 'SECURE_AUTH_SALT', 'yD(mw2zFhppzIj[O_Jd+H{]tN8Z]o<ql`M_X62Q#XjN.t_<$HW#7C7~aT?*95S{X' );
define( 'LOGGED_IN_SALT',   'mwlSTtXNK5[C_={h[aBq`)4!a4,3*K^DeUoEVRH3Ub`cVpd>$c#c?S9v&0ynaAsZ' );
define( 'NONCE_SALT',       ':TX`?(N1v+wWJZQr.8x6g[T +Fu:W!S2m.na#(R`5b->(Rd%&:7M=bd?38L8*uMF' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
